#!/usr/bin/env bash
#
#


#install Ag command-line for CtrlP dependency
sudo apt-get install silversearcher-ag curl

#Powerline fonts install
git clone https://github.com/powerline/fonts.git fonts
cd fonts
./install.sh

#vim plugin setup
mkdir -p ~/.vim/autoload ~/.vim/bundle && \
curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim

git clone https://github.com/scrooloose/nerdtree.git ~/.vim/bundle/nerdtree
git clone https://github.com/ctrlpvim/ctrlp.vim.git ~/.vim/bundle/ctrlp.vim
cd ~/.vim/bundle && \
git clone https://github.com/tpope/vim-sensible.git
cd ~/.vim/bundle && \
git clone --depth=1 https://github.com/vim-syntastic/syntastic.git
git clone https://github.com/vim-airline/vim-airline ~/.vim/bundle/vim-airline
git clone https://github.com/vim-airline/vim-airline-themes ~/.vim/bundle/vim-airline-themes
git clone https://github.com/edkolev/tmuxline.vim ~/.vim/bundle/tmuxline.vim
git clone https://github.com/edkolev/promptline.vim ~/.vim/bundle/promptline.vim
git clone https://github.com/w0ng/vim-hybrid.git ~/.vim/bundle/vim-hybrid.vim
cd ~/.vim
git clone https://github.com/flazz/vim-colorschemes.git bundle/colorschemes
git clone https://github.com/sjl/gundo.vim.git bundle
git clone https://github.com/rking/ag.vim bundle/ag
cd ~/.vim/bundle
git clone git://github.com/tpope/vim-fugitive.git
git clone https://github.com/Xuyuanp/nerdtree-git-plugin.git ~/.vim/bundle/nerdtree-git-plugin
