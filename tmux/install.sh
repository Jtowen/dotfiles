#!/usr/bin/env bash
#
#

#clone and build tmux
mkdir ~/.tempfiles
cd ~/.tempfiles
sudo apt-get install -y -f libevent-dev build-essential ncurses-dev libncurses5-dev gettext autoconf pkg-config
git clone https://github.com/tmux/tmux.git
cd tmux
sh autogen.sh
./configure && make
sudo cp tmux /usr/local/bin/tmux

#cleanup
rm -rf ~/.tempfiles

